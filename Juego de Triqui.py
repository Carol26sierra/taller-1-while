"""Taller 13: 
Realizar la primera iteración del proyecto triqui 
Se espera como mínimo:
- Un avance en el modelo de requisitos (casos de uso: diagramas y documentos)
-Una avance en arquitectura (diagramas de actividad, diagramas de clase) 
-Un avance en desarrollo que contemple alguno de los diagramas de actividad definidos y la arquitectura de clases 
-Un avance en la prueba de funcionalidad desarrollada"""

import random

def pres_1():
    print("---Juego de tres en raya---")
    print("1. Principiante")
    print("2. Avanzado")

    nivel=""

    while nivel != "1" and nivel != "2":
        nivel=input("--->")

    return int(nivel)

def pres_2():
    print("Comienza la ficha O")
    print("Elija: O / X")
    ficha=""
    while ficha != "O" and ficha != "X":
        ficha=input("-->")

    if ficha == "O":
        usuario1="O"
        usuario2="X"
    else:
        usuario1="X"
        usuario2="O"

    return usuario1, usuario2

def mostrar_tablero(tablero):

    print("        |        |        ")
    print("  {}     |   {}    |   {}   ".format(tablero[0], tablero[1], tablero[2]))
    print("        |        |        ")
    print("--------------------------")
    print("        |        |        ")
    print("  {}     |   {}    |   {}   ".format(tablero[3], tablero[4], tablero[5]))
    print("        |        |        ")
    print("--------------------------")
    print("        |        |        ")
    print("  {}     |   {}    |   {}   ".format(tablero[6], tablero[7], tablero[8]))
    print("        |        |        ")

jugando=True
while jugando:

    tablero=[" "]*9
    nivel=pres_1()
    usuario1, usuario2=pres_2()
    mostrar_tablero(tablero)
    if usuario1=="O":
        turno="Usuario1"
    else:
        turno="Usuario2"
    