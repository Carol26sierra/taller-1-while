#Importar el módulo para enviar respuestas al cliente
from django.http import HttpResponse
#Importar el módulo para gestionar vistas
from django.views import View
#se importa el sistema de carga de plantillas
from django.template import loader

#Crear una clase que herede de la clase View
class personaje(View):

   #Crear un método que corresponda a un método definido por 
   # el protocolo HTTP
   def get(self,request):
       #Crear un bjeto que haga referencia a la plantilla
       template=loader.get_template('personaje.html')
       #se crea un diccionario con las variables que requiera la plantilla
       diccionario={
           'nombre':'django'
       }
       pagina=template.render(diccionario)
       response= HttpResponse(pagina)
       return response

