from django.http import HttpResponse
from django.views import View
#Importar el sistema de carga de plantillas
from django.template import loader 
class Saludador(View):
   def get(self,request):
       #Crear un objeto que haga referencia a la plantilla
       template = loader.get_template('saludoador.html')
       #Crear un diccionario con las variables que requiere la plantilla
       diccionario = { 
           'nombre':'Django' 
       } 
       #crear la cadena del texto de respuesta usando lo que esta en la plantilla + los datos del diccionario
       pagina=template.render(diccionario)

       #2.crear un objeto de la clase HttpResponse pasando la cadena de texto
       respuesta= HttpResponse(pagina)

       #Retornar ese objeto
       return respuesta