from django.db.models import *

class Categoria(Model ):

    nombre=CharField(max_length=100)
    descripcion=TextField()

class empresa_de_basura (Model):
    id_categoria=ForeignKey(Categoria, on_delete=PROTECT)
    nombre=CharField(max_length=100)
    correo=EmailField(max_length=100)
    direccion=CharField(max_length=100)
    descripcion=CharField(max_length=100)
    ciudad=CharField(max_length=100)

class reciclador(Model):
    id_categoria=ForeignKey(Categoria, on_delete=PROTECT)
    nombre=CharField(max_length=100)
    edad=IntegerField()
    correo=EmailField()

class sitio_de_recoleccion(Model):
    id_categoria=ForeignKey(Categoria, on_delete=PROTECT)
    localidad=CharField(max_length=100)
    horario=DurationField()
    fecha=DurationField()
    direccion=CharField(max_length=100)

class habitante_de_barrio(Model):
    id_categoria=ForeignKey(Categoria, on_delete=PROTECT)
    nombre=CharField(max_length=100)
    edad=IntegerField()
    direccion=CharField(max_length=100)
    fotografia=ImageField()