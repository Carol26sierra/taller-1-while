# Taller: Dado un conjunto de valores en una lista de 1000 valores generados aleatoriamente
# entre 0 y 20.
# 1. Crear una función que devuelva una lista con valor True o False dependiendo si el valor 
#    de la lista original es primo o no
# 2. Crear una función que devuelva la media de la lista
# 3. Crear una función que devuelva una lista con el factorial de cada uno de los números
# 4. Crear una función que retorne el valor mayor y el menor de la lista

import random

n=1000
NumAleatorio = [random.randint(0,20) for _ in range(n)]
print (NumAleatorio)

#primer punto
def primo(NumAleatorio):
    if NumAleatorio == 1:
        return False
    elif NumAleatorio == 2:
        return True 
    else:
        for i in range (2, NumAleatorio):
            if NumAleatorio % i == 0:
                return False

        return True 

for i in range(1, 1001):
    print(i, primo(i))

#segundo punto
import pandas as pd

df=pd.DataFrame(NumAleatorio)

media=df.mean(axis=0)
print("La media de los numeros aleatorios es: ")
print(media)

#tercer punto
import math

def definir_factorial(NumAleatorio):
    lista_resultado=[]
    for i in NumAleatorio:
        valor=math.factorial(i)
        lista_resultado.append(valor)
    return lista_resultado

miNumAleatorio=NumAleatorio
print(miNumAleatorio)
print(definir_factorial(miNumAleatorio))

#cuarto punto
mayor=max (NumAleatorio)
menor=min (NumAleatorio)
print("El numero mayor es: ",mayor)
print("El numero menor es: ",menor)