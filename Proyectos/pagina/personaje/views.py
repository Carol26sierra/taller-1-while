from django.http import HttpResponse
from django.views import View
from django.template import loader

class personaje(View):
    def get(self, request):
        template=loader.get_template(personaje.html)
        diccionario={
            'nombre':'django'
        }
        pagina=template.render(diccionario)
        response=HttpResponse(pagina)
        return response 