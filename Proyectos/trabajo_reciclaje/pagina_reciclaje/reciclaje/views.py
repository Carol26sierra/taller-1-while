from django.shortcuts import render
from django.views import View
from .forms import FormDatosPersonales
from .forms import FormLocalizacion
from .forms import FormHorario 

class pagina(View):
    def get(self, request):

        datos={
            'titulo':'Punto ECO'
        }
        return render (request, 'principal/plantillaPagina.html', datos)


class DatosPersonalesView(View):
    def procesarDatosPersonales (request):

        miFormulario=FormDatosPersonales()

        datos={

            "form":miFormulario

        }
        return render(request, "plantillaDatosPersonales.html", datos)

class LocalizacionView(View):
    def procesarLocalizacion (request):

        miFormulario=FormLocalizacion()

        datos={

            "form":miFormulario

        }
        return render(request, "plantillaLocalizacion.html", datos)

class HorarioView(View):
    def procesarHorario (request):

        miFormulario=FormHorario()

        datos={

            "form":miFormulario

        }
        return render(request, "plantillaHorario.html", datos)