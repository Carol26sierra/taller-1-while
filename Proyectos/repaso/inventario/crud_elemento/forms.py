#from attr import attrs
from django.forms import CharField, Form, Textarea

class FormElemento(Form):

    #campos del formulario como atributos de clase 

    nombre=CharField(max_length=50)

    #otros campos, por ejemplos: 
    #descripcion=CharField(widget=Textarea(attrs={"rows":10, "cols":10}))