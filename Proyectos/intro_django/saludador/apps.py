from django.apps import AppConfig


class SaludadorConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'saludador'
