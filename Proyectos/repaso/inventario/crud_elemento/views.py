from django.shortcuts import render
from django.views import View
from .forms import FormElemento 

class ElementoView(View):

    #crear el metodo que procesara el formulario
    def procesarElemento(request):

    #crear un objeto de la clase del formuario
        miFormulario=FormElemento()

        datos={

            "form": miFormulario
        }

        return render (request, "plantillaCRUDElemento.html", datos)