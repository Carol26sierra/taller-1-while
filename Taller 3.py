import random 
print ("Digite la cantidad de números que desea obtener aleatoriamente")
n=int(input())
NumAleatorios = [random.randint(-100,100) for _ in range(n)]
print (NumAleatorios)

import pandas as pd

df=pd.DataFrame(NumAleatorios)

mediana=df.median(axis=0)
print("La mediana de los numeros aleatorios es: ")
print(mediana)

media=df.mean(axis=0)
print("La media de los numeros aleatorios es: ")
print(media)